package com.agiletestingalliance;

public class MinMax {

    public static int checkMax(final int firstnum, final int secondnum) {
        return secondnum > firstnum ? secondnum : firstnum;
    }

    public static String bar(final String string) {
        if (!string.isEmpty()) {
            return string;
        }
        return "";
    }
}

